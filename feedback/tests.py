from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Feedback
from django.contrib.auth.models import User
from .forms import Input_Form
from django.apps import apps
from feedback.apps import FeedbackConfig

import json

# Create your tests here.
class FeedTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', email='test@gmail.com', password='12345')
        login = self.client.force_login(self.user)
    
    # Test app
    def test_apps(self):
        self.assertEqual(FeedbackConfig.name, 'feedback')
        self.assertEqual(apps.get_app_config('feedback').name, 'feedback')

    # Test views dan url
    def test_feedback_url(self):
        response = self.client.get('/feedback/')
        self.assertEqual(response.status_code, 200)

    def test_using_feedback_template(self):
        response = self.client.get('/feedback/')
        self.assertTemplateUsed(response, 'feedback/feedback.html')

    def test_using_feedback_function(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, index)

    # Test model
    def test_feedback_model(self):
        Feedback.objects.create(user=self.user, content='a')
        data = Feedback.objects.all().count()
        self.assertEqual(data, 1)

    # Test form
    def test_form_feedback_valid(self):
        form = Input_Form(data={'content':'PPW', 'user':self.user})
        self.assertTrue(form.is_valid())

    def test_form_feedback_invalid(self):
        form= Input_Form(data={})
        self.assertFalse(form.is_valid())

    def test_post_form_feedback_valid(self):
        arg = {'content' : 'test', 'user':self.user}
        response = self.client.post('/feedback/saveFeedback/', arg)
        self.assertEqual(response.status_code, 200) #CEK
        data = Feedback.objects.all().count()
        self.assertEqual(data, 1)

    def test_post_form_feedback_invalid(self):
        arg = {'content' : '', 'user':self.user}
        response = self.client.post('/feedback/saveFeedback/', arg)
        data = Feedback.objects.all().count()
        self.assertNotEqual(data, 1)

    # Test Ajax
    def test_form_and_data_ajax(self):
        arg = {'content' : 'testppw', 'user':self.user}
        response = self.client.post('/feedback/saveFeedback/', arg)
        data = json.loads(response.content)
        self.assertEqual(data,{'items': [{'pk': 1, 'user': 'testuser', 'content': 'testppw'}]})
        response2 = self.client.post('/feedback/')
        isi = response.content.decode('utf8')
        self.assertIn('testppw', isi)
        data = Feedback.objects.all().count()
        self.assertEqual(data, 1)


    def tearDown(self):
        self.user.delete()

