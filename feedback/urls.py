from django.urls import path
from . import views

app_name = 'feedback'
urlpatterns = [
    path('', views.index, name='index'),
    path('saveFeedback/', views.saveFeedback, name='saveFeedback'),
    path('allFeedback/', views.allFeedback, name='allFeedback'),

]
