from django.shortcuts import render
from .forms import Input_Form
from .models import Feedback
from django.contrib.auth.models import User
from django.urls import reverse
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from django.http import JsonResponse

# Create your views here.
def index(request):
    response = {'input_form' : Input_Form}
    return render(request, 'feedback/feedback.html', response)

def saveFeedback(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        feed = form.save(commit=False)
        feed.user = request.user
        feed.save()
        qs = Feedback.objects.all()
        feed_json = []
        for f in qs:
            tmp = {
                    "pk": f.pk,
                    "user": f.user.username,
                    "content": f.content
                }
            feed_json.append(tmp)

        return JsonResponse({'items':feed_json}, safe=False)
    else:
        form = Input_Form()
        return render(request,'feedback/feedback.html')

def allFeedback(request):
    qs = Feedback.objects.all()
    feed_json = []
    for f in qs:
        tmp = {
                "pk": f.pk,
                "user": f.user.username,
                "content": f.content
            }
        feed_json.append(tmp)

    return JsonResponse({'items':feed_json}, safe=False)



