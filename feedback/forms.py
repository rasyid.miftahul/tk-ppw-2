from django import forms
from .models import Feedback

class Input_Form(forms.ModelForm):
    class Meta:
        model = Feedback
        exclude = ('user',)
    content_attrs = {
        'type' : 'text',
        'placeholder' : '',
        'style': 'height: 2em;',
    }

    content = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=content_attrs))