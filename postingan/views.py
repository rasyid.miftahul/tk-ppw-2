from django.shortcuts import render, redirect, HttpResponse
from .forms import FormGambar
from .models import Gambar
from django.core import serializers
import json

# Create your views here.

def index(request):
    gambar = Gambar.objects.all()

    content = {
        "title" : "Pandenemy - Posts",
        "gambars" : gambar
    }

    return render(request, 'postingan/show.html', content)

def upload(request):
    content = {}

    if request.method == 'POST':
        form_gambar = FormGambar(request.POST, request.FILES)
        if form_gambar.is_valid():
            img = form_gambar.cleaned_data.get("file_gambar")
            title = form_gambar.cleaned_data.get("judul")
            desc = form_gambar.cleaned_data.get("deskripsi")
            obj = Gambar.objects.create(file_gambar=img, judul=title, deskripsi=desc)
            obj.save()

            return redirect("/postingan")
    
    content["title"] = "Upload Gambar"
    content["form"] = FormGambar()
    return render(request, 'postingan/upload.html', content)

def detail(request, input_id):
    gambar = Gambar.objects.get(id=input_id)

    content = {
        "title": "Pandenemy - Post",
        "gambar" : gambar
    }

    return render(request, 'postingan/detail.html', content)

def fotoJSON(request, keyword=""):

	listFoto = Gambar.objects.filter(judul__contains=keyword)
	JSONFoto = serializers.serialize("json", listFoto)
	data = json.loads(JSONFoto)
	returned = json.dumps(data)
	return HttpResponse(returned, content_type ="application/json")
        
    
