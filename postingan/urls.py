from django.urls import path, re_path
from . import views


urlpatterns = [
    path('postingan/', views.index, name='index'),
    path('postingan/upload/', views.upload, name='uploadGambar'),
    path('postingan/detail/<int:input_id>', views.detail, name='detail'),
    re_path('fotoDB&keyword=(?P<keyword>[\w-]+)',views.fotoJSON, name='fotoJSON'),
]