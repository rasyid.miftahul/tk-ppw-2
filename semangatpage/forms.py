from django import forms
from .models import kata

class kataForm(forms.ModelForm):
    class Meta:
        model = kata
        fields = '__all__'

        widgets = {
            'katasemangat' : forms.Textarea(attrs={'class': 'form-control', 'placeholder' : 'Kata Semangat'}),
            'author' : forms.TextInput(attrs={'class': 'form-control', 'placeholder' : 'Penulis'})
        }