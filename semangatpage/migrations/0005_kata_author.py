# Generated by Django 3.1 on 2021-01-02 10:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('semangatpage', '0004_remove_kata_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='kata',
            name='author',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
