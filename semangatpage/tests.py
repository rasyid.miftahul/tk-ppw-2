from django.test import TestCase, Client
from django.urls import resolve
from .views import semangat, add_semangat
from .models import kata
from .forms import kataForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.apps import apps
from .apps import SemangatpageConfig


class TestSemangatPage(TestCase):

#urlTest
    def test_url_semangat(self): #GET
        response = Client().get('/semangatpage/')
        self.assertEqual(response.status_code, 200)

    def test_url_addsemangat(self): #GET
        response = Client().get('/semangatpage/addsemangat/')
        self.assertEqual(response.status_code, 302)
    
    def test_url_findauthor(self): #GET
        response = Client().get('/semangatpage/findauthor/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_kata_semangat(self): #POST
        self.user = User.objects.create_user(username='user', email='user@gmail.com', password='user123')
        login = self.client.login(username='user', password='user123')
        response = self.client.post('/semangatpage/addsemangat/', {'katasemangat': 'semangat!', 'author' : 'a'})
        jumlah = kata.objects.filter(katasemangat = 'semangat!', author = 'a').count()
        self.assertEqual(jumlah, 1)
        
    def test_get_addsemangat(self): #GET
        self.user = User.objects.create_user(username='user', email='user@gmail.com', password='user123')
        login = self.client.login(username='user', password='user123')
        response = self.client.get('/semangatpage/addsemangat/')
        self.assertEqual(response.status_code, 200)


#viewsTest
    def test_template_semangat_is_used(self): #test template used
        response = Client().get('/semangatpage/')
        self.assertTemplateUsed(response, 'semangatpage/semangat.html')

    def test_views_semangat(self): #test function
        found = resolve('/semangatpage/')
        self.assertEqual(found.func, semangat)

    def test_views_add_semangat(self): #test function
        found = resolve('/semangatpage/addsemangat/')
        self.assertEqual(found.func, add_semangat)

#modelTest
    def test_cek_model_create_kata(self): #functionality test
        kata.objects.create(katasemangat="semangat")
        jumlah = kata.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_cek_model_return_kata(self): #return test
        kata.objects.create(katasemangat="semangat")
        hasil = kata.objects.get(id=1)
        self.assertEqual(str(hasil), "semangat")

#formTest
    def test_form_semangat_is_valid(self): #form validation test
        form = kataForm(data={'katasemangat':'semangat', 'author' : 'a'})
        self.assertTrue(form.is_valid())

    def test_form_semangat_is_invalid(self): #form invalidation test
        form = kataForm(data={})
        self.assertFalse(form.is_valid())

#ajaxTest
    def test_search_url(self):
        response = Client().get('/semangatpage/findauthor/semangatDB/?keyword=a')
        self.assertEqual(response.status_code, 200)

#appTest
    def app_test(self):
        self.assertEqual(SemangatpageConfig.name, 'semangatpage')
        self.assertEqual(apps.get_app_config('semangatpage').name, 'semangatpage')




    