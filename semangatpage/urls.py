from django.urls import include, path, re_path
from . import views

app_name = 'semangatpage'

urlpatterns = [
    path('', views.semangat, name='semangat'),
    path('addsemangat/', views.add_semangat, name='addsemangat'),
    path('findauthor/', views.find_author, name='findauthor'),
    path('findauthor/semangatDB/',views.datajson, name='datajson'),
]