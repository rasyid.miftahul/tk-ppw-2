from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .forms import kataForm
from .models import kata
from django.contrib.auth.decorators import login_required
from django.core import serializers
import json

def semangat(request):
    semangat = kata.objects.all()
    return render(request, 'semangatpage/semangat.html', {'semangat' : semangat})

@login_required
def add_semangat(request):
    if request.method == 'POST':
        form = kataForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/semangatpage/')    
    else:
        form = kataForm()
        return render(request, 'semangatpage/addsemangat.html', {'form': form})

def datajson(request, keyword=""):
    arg = request.GET['keyword']
    daftarkatasemangat = kata.objects.filter(author__contains=arg)
    katajson = serializers.serialize("json", daftarkatasemangat)
    data = json.loads(katajson)
    return JsonResponse(data, safe=False)

def find_author(request):
    return render(request, 'semangatpage/findauthor.html')
