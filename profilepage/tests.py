from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from django.apps import apps
from .apps import ProfilepageConfig
from .views import indexx

class UrlsTestProfile(TestCase):
    def setUp(self):
        self.indexx = reverse('indexx')

    def test_func_index_profile(self):
        # self.index = reverse('indexx')
        f = resolve(self.indexx)
        self.assertEqual(f.func, indexx)

class ViewsTestProfile(TestCase):
    def setUp(self):
        self.indexx = reverse('indexx')

    def test_GET_index_profile(self):
        response = Client().get(self.indexx)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profilepage/profile.html')

class TestAppProfile(TestCase):
    def test_apps_profile(self):
        self.assertEqual(ProfilepageConfig.name, 'profilepage')
        self.assertEqual(apps.get_app_config('profilepage').name, 'profilepage')
