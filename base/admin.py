from django.contrib import admin
from .models import PoolingBarang, BarangKepunyaan

admin.site.register(PoolingBarang)
admin.site.register(BarangKepunyaan)

# Register your models here.
