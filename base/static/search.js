$("#search").keyup(function () {
    var isi = $("#search").val();
    var urll = "/fotoDB&keyword=" + isi;
    $.ajax({
        url: urll,
        success: function (hasil) {
            console.log(hasil);
            var obj_hasil = $("#foto");
            obj_hasil.empty();
            
            for (index = 0; index < hasil.length; index++) {
                var tmp_gambar = hasil[index].fields.file_gambar;
                var tmp_title = hasil[index].fields.judul;
                var final_image = "<img class=\"card-img-top\" src=\"../../../media/" + tmp_gambar + "\"" + "alt=\"Card image cap\">";
                obj_hasil.append(
                    
                    "<div class=\"card\"  style=\"width: 18rem;\">" +
                        final_image +
                        "<div class=\"card-body\">" +
                            "<p class=\"card-text\" style=\"color: white; font-weight: bold;\">" + tmp_title + "</p>" +
                        "</div>" +
                    "</div>"
                       

                );
            }
        }
    });
});