from django.db import models
from django.contrib.auth.models import User


class PoolingBarang (models.Model):
    # Models barang
    # DIGANTI: biar tiap user punya daftar barangnya sendiri
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    barang = models.CharField('Barang', max_length=20, null=True)
    stok = models.IntegerField('Like', default=0, null=True)


class BarangKepunyaan (models.Model):
    # Models yang diisi daftar barang yang dipunyain user
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    daftarBarang = models.ManyToManyField(PoolingBarang)


# Create your models here.
