from django.shortcuts import render, redirect
from .models import PoolingBarang, BarangKepunyaan
from .forms import loginForm, registerForm
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def index(request):
    if request.user.is_authenticated:
        # return tuple 
        listBarang, created = BarangKepunyaan.objects.get_or_create(user=request.user)
        if created == True:
            masker = PoolingBarang.objects.create(
                user=request.user,
                barang="Masker",
                stok=0
            )
            masker.save()
            sanitizer = PoolingBarang.objects.create(
                user=request.user,
                barang="Sanitizer",
                stok=0
            )
            sanitizer.save()
            faceshield = PoolingBarang.objects.create(
                user=request.user,
                barang="Face Shield",
                stok=0
            )
            faceshield.save()
            listBarang.daftarBarang.add(masker, sanitizer, faceshield)
            listBarang.save() 
            # To create and save an object 

        barang = listBarang.daftarBarang.all()
        map = {
            'barang': barang,
        }
        return render(request, 'base/index.html', map)
    return render(request, 'base/index.html')


def kurangin(request):
    namaBarang = request.POST.get('barang', None)
    # listBarang = BarangKepunyaan.objects.get(user=request.user)
    barang = PoolingBarang.objects.get(user=request.user, barang=namaBarang)
    if barang.stok > 0:
        barang.stok -= 1
        barang.save()
    else:
        response = JsonResponse(
            {"error": "Ayo beli persediaan barang jangan sampai habis"})
        response.status_code = 403
        return response
    return redirect('/')


def tambahin(request):
    namaBarang = request.POST.get('barang', None)
    # listBarang = BarangKepunyaan.objects.get(user=request.user)
    barang = PoolingBarang.objects.get(user=request.user, barang=namaBarang)
    barang.stok += 1
    # print(barang.stok)
    barang.save()
    return redirect('/')


def login_tk2(request):
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data['username']
            input_password = form.cleaned_data['password']
            user = authenticate(
                request, username=input_username, password=input_password)
            if user is not None:
                login(request, user)
                return redirect('/')
            else:
                return redirect('/signup/')
    else:
        form = loginForm()
        response = {'form': form}
        return render(request, 'base/login.html', response)


def logout_tk2(request):
    logout(request)
    return redirect('/')


def signup(request):
    if request.method == "POST":
        form = registerForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data.get("username")
            input_email = form.cleaned_data.get("email")
            input_password = form.cleaned_data.get("password")
            try:
                user = User.objects.get(username=input_username)
                return redirect('/signup/')
            except User.DoesNotExist:
                user = User.objects.create_user(
                    input_username, input_email, input_password)
                return redirect('/login/')
    else:
        form = registerForm()
        response = {'form': form}
        return render(request, 'base/signup.html', response)
