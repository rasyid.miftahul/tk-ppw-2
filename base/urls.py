from django.urls import path
from . import views

app_name = 'base'

urlpatterns = [
    path('', views.index, name='base'),
    # path('/masker', views.masker, name='masker'),
    # path('/sanitizer', views.sanitizer, name='sanitizer'),
    # path('/faceshield', views.faceshield, name='faceshield'),
    path('/add', views.tambahin, name='add'),
    path('/reduce', views.kurangin, name='reduce'),
    path('login/', views.login_tk2, name='login'),
    path('logout/', views.logout_tk2, name='logout'),
    path('signup/', views.signup, name='signup'),

]
