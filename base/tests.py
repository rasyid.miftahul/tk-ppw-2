from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from django.contrib import auth
from .models import PoolingBarang, BarangKepunyaan

class TestBase(TestCase):
    #urltest
    def test_url_TK2(self): #GET
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_TK2_login(self): #GET
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_TK2_register(self): #GET
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    
    # def test_url_add(self): #GET
    #     response = Client().get('/add')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_url_TK2_register(self): #GET
    #     logout = self.client.post('/logout/')
    #     self.assertEqual(logout,'/')
    #     self.client.logout()


    # def test_url_add (self):
    #     response = Client().get('/add')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_url_reduce (self):
    #     response = Client().get('/reduce')
    #     self.assertEqual(response.status_code, 200)
    
#viewsTest
    def test_template_TK2_is_used(self): #test template used
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base/index.html')
    
    def test_template_TK2_login_is_used(self): #test template used
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'base/login.html')
    
    def test_template_TK2_signup_is_used(self): #test template used
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'base/signup.html')

#userTest
    def test_user_TK2(self):
        user = User.objects.create_user('admin', 'admin@gmail.com', 'admin')
        request = Client().get('/')
        request.user = user
        self.assertEqual(request.status_code, 200)

#modeltest
    def test_setupClassPoolingBarang (self):
        userdummy = User.objects.create_user (
            username = "aku",
            email = "aku@gmail.com",
            password = "aku",
        )

        pooling_barang = PoolingBarang.objects.create (
            user = userdummy,
            barang = 'nama_barang',
            stok = 3,
        )
        
        self.assertTrue(isinstance(pooling_barang, PoolingBarang))

#viewTestPooling
    def test_logout (self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)


    def test_index_Pooling (self):
        c = Client()
        user = User.objects.create_user (username='test')
        user.set_password('test')
        user.save()

        c.login (username='test', password = 'test')
        # cart = BarangKepunyaan.create(user = user)

        masker = PoolingBarang.objects.create(
            user = user,
            barang = "Maksker",
            stok = 0
        )

        sanitizer = PoolingBarang.objects.create(
            user = user,
            barang = "Sanitizer",
            stok = 0
        )

        faceshield = PoolingBarang.objects.create(
            user = user,
            barang = "Face Shield",
            stok = 0
        )
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'base/index.html')
    
    def test_Kurangin (self):
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()

        c.login(username='test',password='test')
        # user = auth.get_user(self.client)
        # assert user.is_authenticated
        barang = PoolingBarang.objects.create(user=user)
        c.post('/add/',{'barang': 'Masker'})
        barang = PoolingBarang.objects.get(user=user)
        self.assertEqual(barang.stok, 0)

    def test_Tambahin (self):
        c = Client()
        user = User.objects.create_user(username='test')
        user.set_password('test')
        user.save()

        c.login(username='test',password='test')
        # user = auth.get_user(self.client)
        # assert user.is_authenticated
        barang = PoolingBarang.objects.create(user=user)
        c.post('/add', data=dict(barang='barang'))
        # c.post('/add/',{'barang': 'Sanitizer'})
        barang = PoolingBarang.objects.get(user=user)
        self.assertEqual(barang.stok, 0)