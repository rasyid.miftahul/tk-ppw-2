$("#search").keyup(function () {
    var isi = $("#search").val();
    var urll = "/fotoDB&keyword=" + isi;
    $.ajax({
        url: urll,
        success: function (hasil) {
            var obj_hasil = $("#foto");
            obj_hasil.empty();
            console.log(hasil);
            
            for (index = 0; index < hasil.items.length; index++) {
                var tmp_title = hasil.items[index].volumeInfo.title;
                var tmp_image = hasil.items[index].volumeInfo.imageLinks.smallThumbnail;
                var final_image = "<img src=" + tmp_image + "class=" + "card-img" + ">"
                obj_hasil.append(
                    "<div class=" + "card mb-3" + "style=" + "max-width: 540px;" + ">" +
                        "<div class=" + "row no-gutters" + ">" +
                            "<div class=" + "col-md-4" + " style='margin: auto; display: flex'>" +
                                final_image +
                            "</div>" +
                            "<div class=" + "col-md-8" + ">" +
                                "<div class=" + "card-body" + ">" +
                                    "<h5 class=" + "card-title" + ">" + tmp_title + "</h5>" +
                                    "<p class=" + "card-text" +">" + tmp_description + "</p>" +
                                    "<a tar get='_blank' href=" + tmp_more_on + "class=" + "btn btn-primary" + " style='color: black;'>" + "Read More" + "</a>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>"
                );
            }
        }
    });
});