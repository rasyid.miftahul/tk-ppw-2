$(document).ready(function() {
    // console.log("Masuk")
    $(".tambah").submit(function (e){
        // Agar tidak terefresh
        e.preventDefault();

        var stok = $(this).parent().siblings("#stok").children("#stok-barang");
        var jumlahStok = parseInt(stok.text());
        var namaBarang = this.parentNode.id;
        var urlnya = $(this).attr('action');
        // console.log(namaBarang);
        // console.log(jumlahStok);
        // console.log(url);
        
        $.ajax({
            type: 'POST',
            url: urlnya,
            data: {
                // karena ada csrf di form, maka valuenya juga harus teradapat di Ajax
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                'barang': namaBarang,
            },
            success: function(response){
                stok.text(jumlahStok+1);
                console.log("Masuuuk");
            },
            error: function(response){
                console.log("Gagal");
            },
        });
    });
    
    $(".kurang").submit(function (e){
        e.preventDefault();
        var stok = $(this).parent().siblings("#stok").children("#stok-barang");
        var jumlahStok = parseInt(stok.text());
        var namaBarang = this.parentNode.id;
        var urlnya = $(this).attr('action');
        $.ajax({
            type: 'POST',
            url: urlnya,
            data: {
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
                'barang': namaBarang,
            },
            success: function(response){
                stok.text(jumlahStok-1);
                console.log("Masuuuk");
            },
            error: function(response){
                console.log("Gagal");
                alert(response.responseJSON.error);
            },
        });
    });
});